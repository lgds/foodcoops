#!/usr/bin/env bash
#
# Start odoo with a list of modules to install from modules.txt file
#
# Usage: ./odoo.sh [/path/to/odoo.conf] [database name]
#

set -eo pipefail


if [ -f modules.txt ]; then
    modulesToInstall="$(paste -s -d, modules.txt)"
fi

if [ -z "${1}" ]; then
    odooConfig=odoo.conf
else
    odooConfig="${1}"
fi

if [ -z "${2}" ]; then
    odooDB=foodcoops
else
    odooDB="${2}"
fi

./odoo/odoo.py \
    --config="${odooConfig}" \
    --addons-path=odoo/addons,extra_addons,intercoop_addons,louve_addons,smile_addons \
    --database="${odooDB}" \
    --init="${modulesToInstall}"
