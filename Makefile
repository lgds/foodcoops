SHELL:=/usr/bin/env bash

.PHONY: launch
launch:
	GROUP_ID="$(shell id -g)" USER_ID="$(shell id -u)" docker-compose up
