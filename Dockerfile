FROM python:2-slim
ARG ODOO_VERSION=9.0
# default gid on "personal" laptops
ARG GROUP_ID=1000
# default uid on "personal" laptops
ARG USER_ID=1000

#######################
# System dependencies #
#######################
RUN apt update && \
        apt install -y libjpeg-dev zlib1g-dev libxml2-dev libxslt1-dev libxmlsec1-dev libsasl2-dev python-dev libldap2-dev libssl-dev libpq-dev python-psycopg2 python-passlib nodejs npm postgresql-client-11 git && \
        apt-get clean && rm -rf /var/lib/apt/lists/ && \
        npm install -g less@3 # Less 4 brakes Odoo assets compilation (see lgds/foodcoops!1)

WORKDIR /home/app
RUN echo ${GROUP_ID}
RUN groupadd -g ${GROUP_ID} app &&\
    useradd -l -u ${USER_ID} -g app app &&\
    install -d -m 0755 -o app -g app /home/app
USER app

############################
# Application dependencies #
############################
# - install Odoo
# - install Odoo dependencies
# - Reinstall psycopg2 due to bug with GLIBC
#   (see https://github.com/psycopg/psycopg2-wheels/issues/2)
# - FoodCoops dependencies
#   - XlsxWriter needed by the 'report_xlsx' module
#   - pysftp needed by the 'auto_backup' module
#   - simplejson needed by the 'hw_cashlogy' module
#   - numpy needed by the 'lacagette_breaking' module
RUN git clone -b "$ODOO_VERSION" --single-branch --depth 1 https://github.com/odoo/odoo.git /home/app/odoo && \
        pip install --user -r odoo/requirements.txt && \
        pip install --user psycopg2 --upgrade && \
        pip install --user XlsxWriter pysftp && \
        pip install --user simplejson && \
        pip install --user numpy && \
        rm -rf /home/app/.cache/pip
# - copy repo code
COPY --chown=app . /home/app/
# Basic configuration of Odoo
RUN mkdir -p /home/app/.local/share/Odoo/ && mv odoo.conf /home/app/.local/share/Odoo/odoo.conf

VOLUME ["/home/app/.local/share/Odoo"]
EXPOSE 8069
CMD ["./odoo.sh", "/home/app/.local/share/Odoo/odoo.conf"]
